$(function() {

    bindEventImageUpload();

    $('.event-description').autosize();

    $('.event-date').pickadate({
        min: new Date(),
        selectYears: true,
        selectMonths: true
    });

    $('.event-time').pickatime({
        format: 'HH:i'
    });

    $(document).on('click', '#edit-event-btn', function() {
        $('#edit-event-form').submit();
    });

    $(document).on('mouseenter', '.create-event-image-wrapper', function() {
        $(this).find('.event-image-upload-icon').removeClass('hide');
    });

    $(document).on('mouseleave', '.create-event-image-wrapper', function() {
        $(this).find('.event-image-upload-icon').addClass('hide');
    });

    $(document).on('click', '.event-remove-uploaded-image-icon', function() {
        var imageId = $('#imageId').val();
        $.ajax({
            url: $(this).attr('data-url'),
            data: 'imageId=' + imageId,
            success: function() {
                $('.create-event-image-wrapper').removeClass('has-image');
            }
        })
    })
});

function bindEventImageUpload() {
    $('#event-image-upload').fileupload({
        url: $(this).attr('data-url'),
        pasteZone: null,
        dataType: 'json',
        add: function (e, data) {
            data.submit()
        },
        done: function (e, data) {
            $('.create-event-image-wrapper img').attr('src', data.result.src);
            $('.create-event-image-wrapper').addClass('has-image');
        }
    })
}