$(".birthDate").pickadate({
    max: new Date(),
    selectYears: 50,
    selectMonths: true
});


$(function () {
    $(document).on('blur, change', 'input#email', function () {
        var email = $(this);
        var url = email.attr("data-url");
        var emailError = $(".js-email-error");
        if (email.val() == '') {
            emailError.text(EMAIL_REQUIRED_ERROR);
        } else {
            $.ajax({
                beforeSend: function() {},
                url: url,
                data: "email=" + email.val(),
                type: "post",
                success: function (data) {
                    if (data === "error") {
                        emailError.text(EMAIL_EXISTS_ERROR);
                        email.attr('data-exists', '');
                    } else {
                        email.removeAttr('data-exists', '');
                    }
                }
            })
        }
    })
});
