$(function() {
    $(document).on('click', '.js-subscribe', function(){
        if ($(this).is('[disabled]')) {
            return
        }
        var url = $(this).attr('data-url');
        var alert = $('.alerts .subscriber-exists-alert');
        var alertWrapper = $('.subscriber-exists-alert-wrapper');
        $.ajax({
            url: url,
            success: function(data){
                if (data === 'error') {
                    alertWrapper.empty().append(alert.clone());
                } else {
                    alertWrapper.empty();
                    $('.event-subscribers').append(data);
                }
            }
        })
    })
});
