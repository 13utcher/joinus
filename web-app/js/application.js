$(function() {
    $(document).foundation();

    keepAlive();

    var opts = {
        lines: 12, // The number of lines to draw
        length: 30, // The length of each line
        width: 10, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#2E2F41', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 38, // Afterglow percentage
        shadow: true, // Whether to render a shadow
        hwaccel: true, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: 0, // Top position relative to parent in px
        left: 0 // Left position relative to parent in px
    };
    var loader = $('.loader');
    var spinner = new Spinner(opts);
    var timer;

    $.blockUI.defaults.css = {};
    $.blockUI.defaults.message = '';
    $.blockUI.defaults.overlayCSS = {opacity: 0, cursor: 'auto'};

    jQuery.ajaxSetup({
        beforeSend: function() {
            $.blockUI();
            timer = setTimeout(function() {
                spinner.spin();
                loader.removeClass('hide');
                loader.append(spinner.el);
            }, 500)

        },
        complete: function(){
            clearTimeout(timer);
            spinner.stop();
            loader.addClass('hide');
            $.unblockUI()
        },
        success: function() {}
    });
});

function keepAlive() {
    setInterval(function() {
        $.ajax({
            beforeSend: function() {},
            url: KEEP_ALIVE_URL,
            success: function() {}
        })
    },1000 * 60 * 5)
}

function initTinyMce(selector) {
    $('.event-editor-wrapper').addClass('active');
    tinymce.init({
        selector: selector,
        skin: "joinus",
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste autoresize",
            "emoticons textcolor youtube"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor emoticons | youtube"
    });
}


