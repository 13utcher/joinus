$(function() {
    initDotdotdot();

    $(document).on('click', '.delete-event-btn', function() {
        var url = $(this).attr('data-url');
        var event = $(this).closest('.event-small');
        $.ajax({
            url: url,
            success: function() {
                event.remove();
            }
        });
    });

    $(document).on('click', '.events-filter dd', function() {
        $('.events-filter dd').removeClass('active');
        $(this).addClass('active');
        var url = $(this).find('a').attr('data-url');
        $.ajax({
            url: url,
            success: function(data) {
                var eventsWrapper = $('.event-small-wrapper');
                eventsWrapper.empty();
                eventsWrapper.append(data);
                initDotdotdot();
            }
        })
    })
});

function initDotdotdot() {
    $('.event-small-text').dotdotdot({
        wrap: 'word',
        watch: 'window',
        height: 125
    });
}
