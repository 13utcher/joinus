$(function() {
    bindEventImageUpload();

    $('.event-small-description').autosize();

    $('.event-date').pickadate({
        min: new Date(),
        selectYears: true,
        selectMonths: true
    });

    $('.event-time').pickatime({
        format: 'HH:i'
    });

    $(document).on('click', '#create-event-btn', function() {

        $('#create-event-form').submit();
    });

    $(document).on('click', '#edit-event-btn', function() {
        $('#edit-event-form').submit();
    });

    $(document).on('click', '.event-remove-uploaded-image-icon', function() {
        var imageId = $('.event-image-wrapper').attr('data-id');
        $.ajax({
            url: $(this).attr('data-url'),
            data: 'imageId=' + imageId,
            success: function() {
                $('.event-image-wrapper').removeClass('has-image');
            }
        })
    });

    $(document).on('click', ".event-upload-image-icon", function(){
        if ($(this).closest('.event-image-wrapper').hasClass('has-image')) {
            return
        }
        $('#event-image-upload').click();
    });

    $(document).on('click', '.submit-event-editor', function () {
        var content = tinymce.activeEditor.getContent();
        tinymce.remove();
        $('.editor').text(content);
        $('.event-description').closest('div').append(content);
        $('.event-editor-wrapper').removeClass('active');
        $(this).addClass('hide');
        $('.edit-event-editor').removeClass('hide');
    });

    $(document).on('click', '.edit-event-editor', function () {
        initTinyMce('.editor');
        $('.event-description').empty();
        $(this).addClass('hide');
        $('.submit-event-editor').removeClass('hide');
    })
});

function bindEventImageUpload() {
    $('#event-image-upload').fileupload({
        url: $(this).attr('data-url'),
        pasteZone: null,
        dataType: 'json',
        add: function (e, data) {
            data.submit()
        },
        done: function (e, data) {
            var image = $('.event-image-wrapper img');
            image.removeAttr('src');
            image.attr('src', data.result.src + '?height=200');
            $('.event-image-wrapper').addClass('has-image');
        }
    })
}

