eventCompileStart = { msg ->
    def proc = "git rev-parse HEAD".execute()
    proc.waitFor()
    new FileOutputStream("grails-app/views/_version.gsp", false) << proc.in.text
}