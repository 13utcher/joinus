import groovy.time.TimeCategory
import joinus.Event
import joinus.Role
import joinus.User
import joinus.UserRole
import joinus.model.EventState
import joinus.model.Sex

class BootStrap {

    def init = { servletContext ->

        def userRole = Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER').save(flush: true)
        def adminRole = Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN').save(flush: true)

        if (User.count == 0) {
            def user = new User(email: 'user@joinus.com', firstName: 'user', lastName: 'lastName',
                    password: 'qwerty', birthDate: Date.parse('yyyy-dd-MM','1990-01-01'), sex: Sex.MALE).save(flush: true)
            def user1 = new User(email: 'user1@joinus.com', firstName: 'user1', lastName: 'lastname1',
                    password: 'qwerty', birthDate: Date.parse('yyyy-dd-MM','1989-01-01'), sex: Sex.FEMALE).save(flush: true)
            def user2 = new User(email: 'user2@joinus.com', firstName: 'user2', lastName: 'lastname2',
                    password: 'qwerty', birthDate: Date.parse('yyyy-dd-MM','1988-01-01'), sex: Sex.MALE).save(flush: true)
            def user3 = new User(email: 'user3@joinus.com', firstName: 'user3', lastName: 'lastname3',
                    password: 'qwerty', birthDate: Date.parse('yyyy-dd-MM','1987-01-01'), sex: Sex.FEMALE).save(flush: true)

            UserRole.create user, userRole
            UserRole.create user1, userRole
            UserRole.create user2, userRole
            UserRole.create user3, userRole

            Date today = new Date()
            Date tomorrow;
            Date yesterday;
            Date theDayAfterTomorrow
            use(TimeCategory) {
                tomorrow = today + 1.day
                yesterday = today - 1.day
                theDayAfterTomorrow = yesterday + 1.day
            }
            def event1 = new Event(maxSubscribers: 5, state: EventState.LIVE, owner: user1, name: 'event1', dateCreated: today, eventDate: tomorrow, descriptionSmall: 'Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my shirt with my name printed on it? Do you see a little Asian child with a blank expression on his face sitting outside on a mechanical helicopter that shakes when you put quarters in it? No? Well, that\'s what you see at a toy store. And you must think you\'re in a toy store, because you\'re here shopping for an infant named Jeb.',
                            description: '<h1 style="text-align: center;">Football</h1>\n' +
                                            '<p>Well, the way they make shows is, they make one show. That show\'s called a pilot. Then they show that show to the people who make shows, and on the strength of that one show they decide if they\'re going to make more shows. Some pilots get picked and become television programs. Some don\'t, become nothing. She starred in one of the ones that became nothing.</p>\n' +
                                            '<p>&nbsp;</p>\n' +
                                            '<p><img src="http://backpost.files.wordpress.com/2011/08/sexy-soccer-girls1.jpg" alt="" width="640" height="480" /></p>\n' +
                                            '<p>&nbsp;</p>\n' +
                                            '<p>Everybody welcome!!</p>').save(flush: true)

            event1.addToSubscribers(user2)
            event1.addToSubscribers(user3)

        }

    }
    def destroy = {
    }
}
