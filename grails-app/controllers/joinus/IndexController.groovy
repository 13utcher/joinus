package joinus

class IndexController {

    transient springSecurityService

    def index() {
        def events = Event.getAll()
        def user = springSecurityService.currentUser
        render view: '/index', model: [events: events, user: user]
    }

    def keepAlive() {
        render 'ok'
    }
}
