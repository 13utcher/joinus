package joinus

import joinus.utils.ConversionUtils
import org.apache.commons.logging.LogFactory

class RegistrationController {

    transient springSecurityService

    private static final def log = LogFactory.getLog('grails.app.controllers.joinus.RegistrationController.class')

    def index() {
        render view: '/registration'
    }

    def register() {
        User user = new User()
        bindData(user, params, [exclude: 'birthDate'])
        user.birthDate = ConversionUtils.convertStringToDate(params.birthDate)
        if (user.validate()) {
            user.save(flush: true)
            log.info("User ${user.id} has been created")
            def userRole = Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER').save()
            UserRole.create user, userRole
            log.info("User ${user.id} has got role ${userRole.id}")
            springSecurityService.reauthenticate(user.email, user.password)
            log.info("User ${user.id} has been authenticated")
            redirect uri: "/"
        } else {
            log.error("User ${user.id} has not been validated with errors: ${user.errors.allErrors}")
            render view: '/registration', model: [user: user]
        }
    }

    def checkEmailAvailability() {
        def email = params.email
        User user = User.findByEmail(email)
        if (user != null) {
            render 'error'
        } else {
            render 'success'
        }
    }

//    def unregister() {
//        User user = …
//        Role role = …
//        UserRole.remove user, role
//    }
}
