package joinus

import org.apache.commons.logging.LogFactory

class LoginController {
    private static final def log = LogFactory.getLog('grails.app.controllers.joinus.LoginController.class')
    
    def auth() {
        render view: '/login'
    }
}
