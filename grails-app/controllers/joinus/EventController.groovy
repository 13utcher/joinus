package joinus

import grails.converters.JSON
import joinus.model.EventState
import joinus.utils.ConversionUtils
import org.apache.commons.logging.LogFactory

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class EventController {

    transient springSecurityService

    private static final def log = LogFactory.getLog('grails.app.controllers.joinus.EventController.class')

    def index() {}

    def view() {
        def event = Event.get(params.id)
        def user = springSecurityService.currentUser
        event.views++
        event.save(flush:true)
        render view: "/viewEvent", model: [event: event, user: user]
    }

    def create() {
        def event = new Event()
        def owner = springSecurityService.currentUser
        event.owner = owner
        event.save(flush: true)
        log.info("User ${owner.id} has created event ${event.id} with draft state")
        render view: "/editEvent", model: [event: event, user: owner]
    }

    def delete() {
        def event = Event.get(params.id)
        def user = springSecurityService.currentUser
        if (event.isOwner(user)) {
            event.delete(flush: true)
            log.info("User ${user.id} has deleted event ${event.id}")
            render ''
        } else {
            log.error("User ${user.id} is not allowed to delete event ${event.id}")
            throw new RuntimeException('Delete event error')
        }
    }

    def edit() {
        def event = Event.get(params.id)
        def user = springSecurityService.currentUser
        if (event.isOwner(user)) {
            render view: "/editEvent", model: [event: event, user: user]
        } else {
            log.error("User ${user.id} is not allowed to edit event ${event.id}")
            throw new RuntimeException('Edit event error')
        }
    }

    def save() {
        def user = springSecurityService.currentUser
        def event = Event.get(params.id)
        if (event.isOwner(user)) {
            def image = Image.get(params.imageId)
            event.name = params.name ?: event.name
            event.descriptionSmall = params.descriptionSmall ?: event.descriptionSmall
            event.description = params.description ?: event.description
            event.image = image ?: event.image
            event.maxSubscribers = params.maxSubscribers == '' ? event.maxSubscribers : params.maxSubscribers as int
            def eventDate = ConversionUtils.convertStringToDateAndTime(params.date, params.time)
            event.eventDate = eventDate
            if (event.state == EventState.DRAFT) {
                event.state = EventState.LIVE
                log.info("User ${user.id} has chandged event ${event.id} state from draft to live")
            }
            event.save(flush: true)
            log.info("User ${user.id} has saved event ${event.id}")
            redirect uri: "/"
        } else {
            log.error("User ${user.id} is not allowed to edit event ${event.id}")
            throw new RuntimeException('Update event error')
        }
    }

    def subscribe() {
        Event event = Event.get(params.id)
        User currentUser = springSecurityService.currentUser
        if (event.subscribers.contains(currentUser) || event.owner.equals(currentUser) || event.subscribers.size() == event.maxSubscribers) {
            render 'error'
        } else {
            event.addToSubscribers(currentUser)
            log.info("User ${currentUser.id} has subscribed on event ${event.id}")
            render view: "/_subscriber", model: [subscriber: currentUser]
        }
    }

    def allEvents() {
        def events = Event.getAll()
        def user = springSecurityService.currentUser
        render view: "/_event-small", model: [events: events, user: user]
    }

    def myEvents() {
        def user = springSecurityService.currentUser
        def events = Event.findAllByOwner(user);
        render view: "/_event-small", model: [events: events, user: user]
    }

    def subscribedEvents() {
        def user = springSecurityService.currentUser
        def events = Event.executeQuery('select e from Event e where :subscriber in elements(e.subscribers)',[subscriber: user])
        render view: "/_event-small", model: [events: events, user: user]
    }

    def addImageToEvent() {
        def event = Event.get(params.id)
        def imageInstance = new Image()
        def src = params.image.getBytes()
        imageInstance.src = src
        InputStream inputStream = new ByteArrayInputStream(src)
        BufferedImage image = ImageIO.read(inputStream)
        imageInstance.width = image.getHeight()
        imageInstance.name = params.image.fileItem.name
        imageInstance.type = params.image.getContentType()
        imageInstance.save(flush: true)
        event.image = imageInstance
        event.save(flush: true)
        log.info("Added image ${imageInstance.id} to event ${event.id}")

        def link = [src: createLink(uri : "/file/getImage/${imageInstance .id}"), id: imageInstance.id]

        render link as JSON
    }

    def removeEventImage() {
        def event = Event.get(params.id)
        def image = Image.get(event.image.id)
        event.image = null
        image.delete(flish: true)
        event.save(flush: true)
        log.info("Deleted image ${image.id} from event ${event.id}")
        render ''
    }
}
