package joinus

import grails.converters.JSON
import groovy.time.TimeCategory
import joinus.utils.ImageUtils
import org.apache.commons.logging.LogFactory

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class FileController {

    private static final def log = LogFactory.getLog('grails.app.controllers.joinus.FileController.class')

    def index() {}

    def uploadImage() {
        def imageInstance = new Image()
        def src = params.image.getBytes()
        imageInstance.src = src
        InputStream inputStream = new ByteArrayInputStream(src)
        BufferedImage image = ImageIO.read(inputStream)
        imageInstance.width = image.getHeight()
        imageInstance.name = params.image.fileItem.name
        imageInstance.type = params.image.getContentType()
        imageInstance.save(flush: true)
        log.info("Image ${imageInstance.id} was uploaded")

        def link = [src: createLink(uri : "/file/getImage/${imageInstance .id}"), id: imageInstance.id]

        render link as JSON
    }

    def getImage() {
        Image img = Image.get(params.id)
        def width= params.width ? params.width as int : null
        def height = params.height ? params.height as int : null
        def src = ImageUtils.resizeImage(img, width, height)
        Date currentDate = new Date()
        Date expiry
        use(TimeCategory) {
            expiry = currentDate + 1.year
        }
        response.setContentType(img.type)
        response.setContentLength(src.size())
        response.setDateHeader("Expires", expiry.getTime())
        response.setHeader("Cache-Control", "max-age="+ expiry.getTime())
        response.setDateHeader("Last-Modified", currentDate.getTime())
        OutputStream out = response.getOutputStream()
        out.write(src)
        out.close()
    }

    def removeImage() {
        def img = Image.get(params.imageId)
        img.delete(flush: true)
        log.info("Image ${img.id} was removed")
        render ''
    }
}
