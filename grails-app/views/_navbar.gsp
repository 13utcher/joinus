<div class="contain-to-grid">
    <nav class="top-bar" data-topbar>
        <ul class="title-area">
            <li class="name">
                <h1><a href="<g:createLink uri='/'/>">JOINUS</a></h1>
            </li>
            <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li>
                    <a href="<g:createLink controller="event" action="create"/>">Create event</a>
                </li>
                <li>
                    <a href="#">${user.fullName()}</a>
                </li>
                <li>
                    <a href='${request.contextPath}/j_spring_security_logout'>
                        <i class="logout-btn fi-power" title="logout"></i>
                    </a>
                </li>
            </ul>

            <!-- Left Nav Section -->
            <ul class="left">
                <li></li>
            </ul>
        </section>
    </nav>
</div>

