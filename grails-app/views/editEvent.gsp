<%@ page contentType="text/html;charset=UTF-8" %>
<g:render template="/header"/>
<div class="columns small-12 small-centered">
    <div class="row">
        <form data-abide action="<g:createLink action="save" controller="event" id="${event.id}"/>" method="post" id="create-event-form">
            <div class="column small-11">
                <div class="row">
                    <div class="columns small-8">
                        <input type="text" id="name" name="name" class="create-event-name" placeholder="Event name" value="${event.name}" required>
                        <small class="error">Name is required</small>
                        <hr>
                        <textarea class="event-small-description" id="descriptionSmall" name="descriptionSmall" placeholder="Event description" required>${event.descriptionSmall}</textarea>
                        <small class="error">Description is required</small>
                    </div>
                    <div class="columns small-4 event-image-wrapper text-center ${event.image ? 'has-image' : ''}" data-id="${event.image?.id}">
                        <img src="${event.image ? createLink(controller: "file", action: "getImage", id: event.image.id, params: [height: 200]) : ''}"/>
                        <i class="fi-upload event-upload-image-icon"></i>
                        <i class="foundicon-remove event-remove-uploaded-image-icon hide" data-url="<g:createLink controller="event" action="removeEventImage" id="${event.id}"/>" title="remove"></i>
                        <input type="file" id="event-image-upload" name="image" data-url="<g:createLink controller="event" action="addImageToEvent" id="${event.id}"/>">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="columns small-1">
                        <label for="date" class="right inline">Date</label>
                    </div>
                    <div class="columns small-3 left">
                        <input type="text" id="date" name="date" class="event-date" placeholder="Enter event date" value="<g:formatDate format="d MMMM, yyyy" locale="${Locale.ENGLISH}" date="${event.eventDate}"/>" required/>
                        <small class="error">Event date is required</small>
                    </div>
                    <div class="columns small-1">
                        <label for="time" class="right inline">Time</label>
                    </div>
                    <div class="columns small-3 left">
                        <input type="text" id="time" name="time" class="event-time" placeholder="Enter event time" value="<g:formatDate format="HH:mm" date="${event.eventDate}"/>" required/>
                        <small class="error">Event time is required</small>
                    </div>
                    <div class="columns small-1">
                        <label for="max" class="right inline">Max</label>
                    </div>
                    <div class="columns small-3 left">
                        <input type="text" id="max" name="maxSubscribers" placeholder="Max" value="${event.maxSubscribers ?: ''}"/>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="column small-12 event-editor-wrapper active">
                        <div class="row event-description"></div>
                        <textarea name="description" class="editor">${event.description}</textarea>
                    </div>
                </div>
            </div>
            <div class="columns small-1">
                <ul data-magellan-expedition="fixed" class="no-bullet event-controls">
                    <li>
                        <i class="fi-save" id="create-event-btn" title="Save"></i>
                    </li>
                    <li>
                        <a href="<g:createLink controller="index" action="index"/>">
                            <i class="fi-home" title="Home"></i>
                        </a>
                    </li>
                    <li>
                        <i class="fi-check submit-event-editor" title="Apply description"></i>
                    </li>
                    <li>
                        <i class="fi-wrench hide edit-event-editor" title="Change description"></i>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</div>

<g:render template="/scripts"/>
<script src="${resource(dir: 'js/datepicker', file: 'picker.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/datepicker', file: 'picker.date.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/datepicker', file: 'picker.time.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/fileupload/vendor', file: 'jquery.ui.widget.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/fileupload', file: 'jquery.iframe-transport.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/fileupload', file: 'jquery.fileupload.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js', file: 'create.js')}?v=<g:render template="/version"/>"></script>
<g:render template="/footer"/>
