<g:render template="/header"/>
<div class="column small-12 small-centered">
    <div class="row">
        <div class="columns small-4 events-title">
            <span>Events</span>
        </div>
        <div class="columns small-8">
            <dl class="sub-nav events-filter right">
                <dd class="active"><a href="javascript:void(0)" data-url="<g:createLink controller="event" action="allEvents"/>">All</a></dd>
                <dd><a href="javascript:void(0)" data-url="<g:createLink controller="event" action="myEvents"/>">Mine</a></dd>
                <dd><a href="javascript:void(0)" data-url="<g:createLink controller="event" action="subscribedEvents"/>">Subscribed</a></dd>
            </dl>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="column">
            <ul class="event-small-wrapper no-bullet">
                <g:render template="/event-small" model="[events: events, user: user]"/>
            </ul>
        </div>
    </div>
</div>
<g:render template="/scripts"/>
<script src="${resource(dir: 'js', file: 'main.js')}?v=<g:render template="/version"/>"></script>
<g:render template="/footer"/>
