<%@ page contentType="text/html;charset=UTF-8" %>
<div class="loader hide"></div>
<div class="alerts">
    <div data-alert class="alert-box warning radius subscriber-exists-alert">
        You have already subscribed on this event!
        <a href="#" class="close">&times;</a>
    </div>
</div>
</body>
</html>