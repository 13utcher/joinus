</div> <!--close Content div-->
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.abide.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.alert.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.clearing.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.dropdown.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.reveal.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.topbar.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.magellan.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.tooltip.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/autosize-master', file: 'jquery.autosize.min.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/blockui-master', file: 'jquery.blockUI.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/spin', file: 'spin.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js', file: 'application.js')}?v=<g:render template="/version"/>"></script>