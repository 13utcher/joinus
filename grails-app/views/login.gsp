<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>JoinUs</title>
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'foundation.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}?v=<g:render template="/version"/>"/>
    <script src="${resource(dir: 'foundation/js', file: 'modernizr.js')}?v=<g:render template="/version"/>"></script>
</head>

<body>
<div class="row">
    <div class="columns small-4 small-centered text-center">
        <img class="login-icon" src="${resource(dir: 'images', file: 'joinus_login_icon.png')}">
    </div>
</div>

<div class="row login-form">
    <div class="columns small-6 medium-6 large-4 small-centered">
        <form id='loginForm' data-abide action='${request.contextPath}/j_spring_security_check' method='POST'>
            <div class="row">
                <label>Email</label>
                <input type='text' name='j_username' id='username' required/>
                <small class="error">An email address is required.</small>
            </div>
            <div class="row">
                <label>Your password</label>
                <input type='password' name='j_password' id='password' required>
                <small class="error">Password is required</small>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <div class="text-center">
                        <input type='checkbox' name='_spring_security_remember_me' id='remember_me'/> remember me
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns text-center">
                    <button type="submit">Join Us!</button>
                </div>
            </div>
            <div class="row">
                <div class="small-6 columns text-left">
                    <a href="${request.contextPath}/forgotAPassword">forgot a password</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </div>
                <div class="small-6 columns text-right">
                    <a href="${request.contextPath}/registration">registration</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="${resource(dir: 'foundation/js/vendor', file: 'jquery.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js', file: 'foundation.min.js')}?v=<g:render template="/version"/>"></script>
<script>
    $(document).foundation();
</script>
</body>
</html>