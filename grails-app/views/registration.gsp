<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>JoinUs</title>
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'foundation.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet"
          href="${resource(dir: 'foundation/icons/stylesheets', file: 'accessibility_foundicons.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'foundation/icons/stylesheets', file: 'foundation-icons.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'js/datepicker/themes', file: 'classic.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'js/datepicker/themes', file: 'classic.date.css')}?v=<g:render template="/version"/>"/>

    <g:render template="/app-config"/>

    <script src="${resource(dir: 'foundation/js', file: 'modernizr.js')}?v=<g:render template="/version"/>"></script>
    <script src="${resource(dir: 'foundation/js/vendor', file: 'jquery.js')}?v=<g:render template="/version"/>"></script>
    <script src="${resource(dir: 'js/dotdotdot', file: 'jquery.dotdotdot.js')}?v=<g:render template="/version"/>"></script>
</head>

<body>

<div class="row registration-form">
    <div class="columns small-4 large-3 small-centered">
        <form id='registerForm' data-abide action='<g:createLink action="register" controller="registration"/>'
              method='POST'>

            <div class="row">
                <div class="small-12 columns text-center">
                    <label><h1>Registration</h1></label>
                </div>
            </div>

            <div class="row">
                <div class="small-6 columns">
                    <input type='text' name='firstName' value="${fieldValue(bean: user, field: 'firstName')}"
                           id='firstName' placeholder='First Name' required/>
                    <small class="error">First name is required.</small>
                </div>

                <div class="small-6 columns">
                    <input type='text' name='lastName' value="${fieldValue(bean: user, field: 'lastName')}"
                           id='lastName' placeholder='Last Name' required/>
                    <small class="error">Last name is required.</small>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns">
                    <input type='email' name='email' id='email' placeholder='E-mail' data-url="<g:createLink controller="registration" action="checkEmailAvailability"/>" required autocomplete="off"/>
                    <small class="error js-email-error">${message(code: 'email.required.error')}</small>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns">
                    <input type='password' name='password' id='password' placeholder='Password' required>
                    <small class="error">Password is required</small>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns text-center">
                    <input type='text' name='birthDate' value="${fieldValue(bean: user, field: 'birthDate')}"
                           id='birthDate' class='birthDate' placeholder="Birthdate"
                           required/>
                    <small class="error">Valid date is required</small>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns text-center">
                    <input type="radio" name="sex" value="MALE" id="male" required>
                    <label for="male"><i class="fi-male" title="male"></i></label>
                    <input type="radio" name="sex" value="FEMALE" id="female" required>
                    <label for="female"><i class="fi-female" title="female"></i>
                </label>
                    <small class="error">Choose your gender</small>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns text-center">
                    <g:renderErrors bean="${user}"/>
                </div>
            </div>

            <div class="row">
                <div class="small-12 columns text-center">
                    <button id="registerButton" type="submit">
                        Join Us!
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="${resource(dir: 'foundation/js/vendor', file: 'jquery.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.abide.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.alert.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.clearing.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.dropdown.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.reveal.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'foundation/js/foundation', file: 'foundation.topbar.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/datepicker', file: 'picker.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js/datepicker', file: 'picker.date.js')}?v=<g:render template="/version"/>"></script>
<script src="${resource(dir: 'js', file: 'registration.js')}?v=<g:render template="/version"/>"></script>
<script>
    $(document).foundation();
</script>
</body>
</html>