<div class="event-small-controls">
    <ul class="no-bullet">
        <li>
            <a data-url="<g:createLink action="delete" controller="event" id="${event.id}"/>" class="delete-event-btn">
                <i class="foundicon-remove" title="delete"></i>
            </a>
        </li>
        <li>
            <a href="<g:createLink action="edit" controller="event" id="${event.id}"/>">
                <i class="foundicon-edit" title="edit"></i>
            </a>
        </li>
    </ul>
</div>