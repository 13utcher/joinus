<%@ page import="joinus.model.EventState" %>
<g:each in="${events}" var="event">
    <g:if test="${event.state != EventState.DRAFT || event.isOwner(user)}">
        <li class="event-small">
            <div class="row">
                <div class="columns small-12">

                    <div class="row">
                        <div class="columns small-2 text-center">
                            <div class="row">
                                <div class="columns small-11">
                                    <img class="small-event-image" src="${event.image ? createLink(controller: "file", action: "getImage", id: event.image.id, params: [height: 100]) : resource(dir: 'images', file: 'event-small-icon.png')}">
                                </div>
                                <div class="columns small-1 has-divider">
                                    <div class="divider"></div>
                                </div>
                            </div>
                        </div>
                        <div class="columns small-7 text-justify">
                            <div class="row">
                                <div class="column small-event-info">
                                    <span>${event.name}</span>
                                    <br>
                                    <span class="text-overflow">
                                        <i data-tooltip class="fi-info has-tip" title="${event.descriptionSmall}"></i>
                                        ${event.descriptionSmall}
                                    </span>
                                    <a class="small-event-read-more" href="<g:createLink controller="event" action="view" id="${event.id}"/>">Read more>></a>
                                </div>
                            </div>
                        </div>
                        <div class="columns small-3">
                            <div class="columns small-1 has-divider">
                                <div class="divider"></div>
                            </div>
                            <div class="columns small-11 small-event-info">
                                <span>
                                    <i class="fi-clock"></i>
                                    <g:formatDate format="yyyy-MMM-dd HH:mm" locale="${Locale.ENGLISH}" date="${event.eventDate}"/>
                                </span>
                                <br>
                                <span>
                                    <i class="fi-torso"></i>
                                    ${event.owner.fullName()}
                                </span>
                                <br>
                                <span>
                                    <i class="general foundicon-location" style="margin-left: -2px"></i>
                                    location
                                </span><br>
                                <i class="fi-torsos-all subscribers-progress-icon" title="subscribers"></i>
                                <g:if test="${event.maxSubscribers == 0}">
                                    <span style="margin-left: 5px">${event.subscribers.size()}</span>
                                </g:if>
                                <g:else>
                                    <div class="progress small-11 subscribers-progress-bar">
                                        <span class="meter" style="width: ${event.getSubscribersPercent()}%">${event.subscribers.size()}/${event.maxSubscribers}</span>
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>
                </div>
                <g:if test="${event.isOwner(user)}">
                    <g:render template="/event-controls" model="[event: event]"/>
                </g:if>
            </div>
            <hr>
        </li>
    </g:if>
</g:each>
