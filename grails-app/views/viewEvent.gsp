<%@ page contentType="text/html;charset=UTF-8" %>
<%@page defaultCodec="none" %>
<g:render template="/header"/>
<div class="columns small-12 view-event-content small-centered">
    <g:set var="isAllowedToSubscribe" value="${event.isAllowedToSubscribe(user)}"/>
    <div class="row">
    <div class="columns small-8">
        <div class="row">
            <div class="column">
                <div class="row">
                    <div class="column">
                        <div class="row subscriber-exists-alert-wrapper"></div>
                        <div class="row">
                            <div class="columns small-3">
                                <a href="<g:createLink uri="/"/>" class="button radius small left view-event-control-btn">Back</a>
                            </div>
                            <div class="columns small-6 text-center view-event-name">
                                <span>${event.name}</span>
                            </div>
                            <div class="columns small-3">
                                <a class="button radius small right view-event-control-btn js-subscribe ${isAllowedToSubscribe ? 'success' : 'secondary'}" ${isAllowedToSubscribe ? '' : 'disabled'} data-url="<g:createLink controller="event" action="subscribe" id="${event.id}"/>">Subscribe</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="columns small-12">
                                ${event.description}
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="column">
                        <span>Date: <span><g:formatDate format="yyyy-MM-dd HH:mm" date="${event.eventDate}"/></span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="columns small-4 text-center">
        <span>Subscribers</span>
        <hr>
        <ul class="no-bullet event-subscribers">
            <g:if test="${event.isOwner(user)}">
                <span class="label success radius">You are event owner</span>
            </g:if>
            <g:each in="${event.subscribers}" var="subscriber">
                <g:render template="/subscriber" model="[subscriber: subscriber]"/>
            </g:each>
        </ul>
    </div>
    </div>
</div>
<g:render template="/scripts"/>
<script src="${resource(dir: 'js', file: 'view.js')}?v=<g:render template="/version"/>"></script>
<g:render template="/footer"/>