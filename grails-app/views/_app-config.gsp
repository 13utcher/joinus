<script>
    //Errors
    var EMAIL_REQUIRED_ERROR = "${message(code: 'email.required.error')}";
    var EMAIL_EXISTS_ERROR = "${message(code: 'email.exists.error')}";
    //Urls
    var KEEP_ALIVE_URL = "<g:createLink controller="index" action="keepAlive"/>";
</script>