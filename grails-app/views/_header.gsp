<%@ page contentType="text/html;charset=UTF-8" %>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>JoinUs</title>
    <link rel="stylesheet" href="${resource(dir: 'foundation/css', file: 'foundation.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'foundation/icons/stylesheets', file: 'general_foundicons.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'foundation/icons/stylesheets', file: 'accessibility_foundicons.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'foundation/icons/stylesheets', file: 'foundation-icons.css')}?v=<g:render template="/version"/>"/>
    <link rel="stylesheet" href="${resource(dir: 'js/datepicker/themes', file: 'classic.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'js/datepicker/themes', file: 'classic.date.css')}?v=<g:render template="/version"/>" />
    <link rel="stylesheet" href="${resource(dir: 'js/datepicker/themes', file: 'classic.time.css')}?v=<g:render template="/version"/>" />

    <g:render template="/app-config"/>

    <script src="${resource(dir: 'foundation/js', file: 'modernizr.js')}?v=<g:render template="/version"/>"></script>
    <script src="${resource(dir: 'foundation/js/vendor', file: 'jquery.js')}?v=<g:render template="/version"/>"></script>
    <script src="${resource(dir: 'js/dotdotdot', file: 'jquery.dotdotdot.js')}?v=<g:render template="/version"/>"></script>
    <script src="${resource(dir: 'js', file: 'application.js')}?v=<g:render template="/version"/>"></script>
    <script type="text/javascript" src="${resource(dir: 'tinymce', file: 'tinymce.min.js')}"></script>
    <script type="text/javascript">
        initTinyMce('.editor');
    </script>
</head>
<body>
<div class="row content">
<g:render template="/navbar"/>