package joinus

import joinus.model.EventState

class Event {

    String name
    String description
    String descriptionSmall
    Date eventDate
    Date dateCreated
    List<User> subscribers = []
    Long views = 0
    EventState state = EventState.DRAFT
    Image image
    User owner
    int maxSubscribers = 0 // 0 = no limit

    static hasMany = [
            subscribers: User
    ]

    //static hasOne = [
    //        owner: User
    //]

    static mapping = {
        subscribers cascade: 'none'
        owner cascade: 'none'
        image cascade: 'delete'
    }

    static constraints = {
        name(nullable: true)
        eventDate(nullable: true)
        dateCreated(nullable: true)
        descriptionSmall(maxSize: 1000, nullable: true)
        description(maxSize: 10000, nullable: true)
        image(nullable: true)
    }

    boolean isOwner(def user) {
        if (owner == user) {
            return true
        } else {
            return false
        }
    }

    boolean isAllowedToSubscribe(def user) {
        if (this.isOwner(user) || this.subscribers.contains(user) || this.subscribers.size() == this.maxSubscribers) {
            return false
        } else {
            return true
        }
    }

    def getSubscribersPercent() {
        this.subscribers.size() == 0 ? 0 : this.subscribers.size() / maxSubscribers * 100
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Event event = (Event) o

        if (dateCreated != event.dateCreated) return false
        if (descriptionSmall != event.descriptionSmall) return false
        if (maxSubscribers != event.maxSubscribers) return false
        if (eventDate != event.eventDate) return false
        if (image != event.image) return false
        if (name != event.name) return false
        if (owner != event.owner) return false
        if (state != event.state) return false
        if (views != event.views) return false

        return true
    }

    int hashCode() {
        int result
        result = (name != null ? name.hashCode() : 0)
        result = 31 * result + (descriptionSmall != null ? descriptionSmall.hashCode() : 0)
        result = 31 * result + (maxSubscribers != null ? maxSubscribers.hashCode() : 0)
        result = 31 * result + (eventDate != null ? eventDate.hashCode() : 0)
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0)
        result = 31 * result + (views != null ? views.hashCode() : 0)
        result = 31 * result + (state != null ? state.hashCode() : 0)
        result = 31 * result + (image != null ? image.hashCode() : 0)
        result = 31 * result + (owner != null ? owner.hashCode() : 0)
        return result
    }
}
