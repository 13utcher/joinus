package joinus

class Image {

    byte[] src
    String name
    int width
    int height
    String type

    static constraints = {
        src(maxSize: 1024*1024*1024*5) //5mb
    }

    static mapping = {
        table 'joinus_image'
        src lazy: true
    }
}
