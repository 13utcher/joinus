package joinus

import joinus.model.Sex

class User {

    transient springSecurityService

    String email
    String password
    String firstName
    String lastName
    Date birthDate
    Sex sex
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    Image image

    static transients = ['springSecurityService']

    static hasMany = [
            subscriptions: Event,
            ownedEvents: Event
    ]

    static mappedBy = [
            subscriptions: "subscribers",
            ownedEvents: "owner"
    ]

    static belongsTo = Event


    static constraints = {
        subscriptions(nullable: true)
        email (blank: false, unique: true)
        password blank: false
        image(nullable: true)
        sex(blank: false)
        firstName(blank: false)
        lastName(blank: false)
        birthDate(blank: false, max: new Date())
    }

    static mapping = {
        password column: '`password`'
        image cascade: 'delete'
        table 'users'
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }

    def fullName() {
        "${this.firstName} ${this.lastName}"
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false
        User user = (User) o
        if (accountExpired != user.accountExpired) return false
        if (accountLocked != user.accountLocked) return false
        if (enabled != user.enabled) return false
        if (passwordExpired != user.passwordExpired) return false
        if (birthDate != user.birthDate) return false
        if (email != user.email) return false
        if (firstName != user.firstName) return false
        if (image != user.image) return false
        if (lastName != user.lastName) return false
        if (password != user.password) return false
        if (sex != user.sex) return false
        return true
    }

    int hashCode() {
        int result
        result = (email != null ? email.hashCode() : 0)
        result = 31 * result + (password != null ? password.hashCode() : 0)
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0)
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0)
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0)
        result = 31 * result + (sex != null ? sex.hashCode() : 0)
        result = 31 * result + (enabled ? 1 : 0)
        result = 31 * result + (accountExpired ? 1 : 0)
        result = 31 * result + (accountLocked ? 1 : 0)
        result = 31 * result + (passwordExpired ? 1 : 0)
        result = 31 * result + (image != null ? image.hashCode() : 0)
        return result
    }


    @Override
    public java.lang.String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", sex=" + sex +
                ", enabled=" + enabled +
                ", accountExpired=" + accountExpired +
                ", accountLocked=" + accountLocked +
                ", passwordExpired=" + passwordExpired +
                ", image=" + image +
                ", version=" + version +
                ", subscriptions=" + subscriptions +
                '}';
    }
}
