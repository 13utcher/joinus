package joinus.utils

import java.text.SimpleDateFormat

public class ConversionUtils {
    public static Date convertStringToDate(String dateString) {
        Date eventDate = new SimpleDateFormat('d MMMM, yyyy', Locale.ENGLISH).parse(dateString)
    }

    public static Date convertStringToDateAndTime(String dateString, String timeString) {
        Date eventDate = new SimpleDateFormat('d MMMM, yyyyHH:mm', Locale.ENGLISH).parse(dateString + timeString)
    }
}
