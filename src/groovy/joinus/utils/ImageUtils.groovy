package joinus.utils

import org.imgscalr.Scalr

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

class ImageUtils {

    static def resizeImage(image, width, height) {
        InputStream inputStream = new ByteArrayInputStream(image.src)
        BufferedImage bufferedImage = ImageIO.read(inputStream)

        def imgWidth = bufferedImage.width
        def imgHeight = bufferedImage.height
        def mode = Scalr.Mode.FIT_EXACT
        if (!width ) {
            width  = imgWidth
            mode = Scalr.Mode.FIT_TO_HEIGHT
        }

        if (!height) {
            height = imgHeight
            mode = Scalr.Mode.FIT_TO_WIDTH
        }
        BufferedImage resized =  Scalr.resize(bufferedImage, Scalr.Method.AUTOMATIC, mode, width, height, Scalr.OP_ANTIALIAS, Scalr.OP_BRIGHTER)
        bufferedImage.flush()
        ByteArrayOutputStream baos = new ByteArrayOutputStream()
        ImageIO.write(resized, 'jpg', baos )
        baos.flush()
        byte[] imageInByte = baos.toByteArray()
        baos.close()
        inputStream.close()
        imageInByte
    }
}
