package joinus.model

public enum EventState {
    DRAFT,
    LIVE
}