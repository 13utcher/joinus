package joinus.model

public enum Sex {
    MALE,
    FEMALE
}
